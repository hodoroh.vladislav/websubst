# TP convoqué du 12 décembre 2019

## Général
Cloner le [repository GIT](https://gitlab.unistra.fr/marco/progweb2-tp-sujet) dans le dossier **/var/www/html/<nom d'utilisateur>/progweb2-tp-sujet**.  

## Consignes
Terminer le projet de site pour le festival de jeu :  
* Dans le fichier "index.php" :  
    * À la ligne 37, créer un affichage pour les articles d'actualité contenus dans la base de données. Il est nécessaire de faire un système responsive :
        * 960px < Écran, 3 articles/lignes ;
        * 720px < Écran < 960px, 2 articles/lignes ;
        * Écran < 720px, 1 article/lignes ;
* Dans le fichier "css/actu.css", compléter le CSS pour que l'affichage des articles d'actualités ressemblent aux captures d'écran ci-jointes.
* Dans le fichier "js/mouseover.js", créer les fonctions Javascript pour faire du bouton "#to__top" un bouton de retour vers le haut de la page.
* Bonus : Dans le fichier "contact.php", faire le traitement du formulaire créé dans "index.php".  

Des indications et des astuces sont ajoutées en commentaires dans les différents fichiers.  
*Il est __interdit__ d'utiliser bootstrap et JQuery.*  

## Technologies
Il vous sera demandé d'utiliser dans votre travail les technologies suivantes :  
* CSS :
    * Grid
    * Flex
* Javascript
    * addEventListener
    * querySelectorAll
* PHP
    * PDO::query
    * PDO::prepare 

#### Articles
![Image des articles](http://www.jojotique.fr/1.png)


![Image des articles](http://www.jojotique.fr/2.png)

Informations sur la table "*actuality*" créée :
* id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
* author VARCHAR(50) NOT NULL,
* title VARCHAR(100) NOT NULL,
* text TEXT NOT NULL,
* date DATETIME NOT NULL

#### Formulaire
Informations sur la table "*contact*" créée :
* id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
* first_name VARCHAR(50) NOT NULL,
* last_name VARCHAR(50) NOT NULL,
* subject INTEGER NOT NULL,
* message TEXT NOT NULL,
* date DATETIME NOT NULL

## Divers
Vous avez accès aux cours et aux documentations habituelles sur le net.

  
Pour la remise du travail, envoyez via Moodle une archives avec l'ensemble des fichiers.
