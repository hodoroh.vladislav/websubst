const toTopButton = document.getElementById('to__top')
toTopButton.addEventListener('click', function (event) {
  event.preventDefault()
  window.scroll(0, 0)
})
