/*
Utiliser l'écouteur d'évènement pour écouter le "mouseover" = survol d'un élément par la souris.
Lorsque la souris est au-dessus d'un article mettre un "box-shadow: 2px 2px 5px black;" et un "background" avec la couleur "#bdbdbd"
 */

//select all articles
let articles = document.querySelectorAll(".articles article");
// console.log(articles);

// mouseover action
articles.forEach(function (item,idx) {
    item.addEventListener('mouseover',function (event) {
        this.style = "box-shadow: 2px 2px 5px black;";
        this.style = "background:#bdbdbd;";
    })
});

// mouseout action
articles.forEach(function (item,idx) {
    item.addEventListener('mouseout',function (event) {
        this.style = "box-shadow:none";
        this.style = "background:none;";
    })
});


