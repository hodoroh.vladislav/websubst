<?php

include_once "formConnection.php"; // Insertion du fichier qui crée l'objet $pdo

/*
Insérer les éléments récupérés du formulaire dans la base de données.

Astuce pour la base de données :
Ne pas oublier qu'une erreur courante d'utilisation d'un fichier "*.db" est qu'il faut ouvrir les droits d'utilisation à tous
pour le fichier et le dossier qui le contient.

N.B. : faire attention à la faille XSS et à la cohérence des données reçues.
*/
if (
    !empty($_POST) &&
    !empty($_POST['firstName']) &&
    !empty($_POST['lastName']) &&
    !empty($_POST['subject']) &&
    !empty($_POST['message'])
){
    // la date normalement se fait d'une manierre automatique, si non, on le cree avec new DateTime();
    $sql = "insert into contact (first_name, last_name, subject, message) values (?,?,?,?)";
    $pdo
        ->prepare($sql)
        ->execute([$_POST['firstName'],$_POST['lastName'],$_POST['subject'],$_POST['message']]);

}
else{
    echo "Empty";
    // fictif location to an error page ....
    header('Location:/'); // Redirection vers la page principal

}
header('Location:/'); // Redirection vers la page principal
