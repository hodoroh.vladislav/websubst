<?php include_once 'formConnection.php'; // Insertion de l'objet $pdo
$tt = $pdo->query("select * from actuality");
var_dump();

?>




<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0"/>
    <title>Prog web 2 - TP noté</title>
    <link rel="icon" type="image/png" href="./img/favicon.png"/>
    <link rel="stylesheet" href="./css/style.css"/>
    <link rel="stylesheet" href="./css/actu.css"/>
    <link rel="stylesheet" href="./css/contact.css"/>
    <link href="https://fonts.googleapis.com/css?family=Comfortaa|Exo+2&display=swap" rel="stylesheet"/>
    <link rel="stylesheet" href="https://cdn.materialdesignicons.com/4.5.95/css/materialdesignicons.min.css"/>
</head>
<body>

<nav>
    <ul>
        <li><a href="#actu">Actualités</a></li>
        <li><a href="#info">Informations</a></li>
        <li><a href="#contact">Contact</a></li>
    </ul>
</nav>

<header id="svg__header">
    <svg viewBox="0 0 360 30">
        <path d="M0,30h360v-30Z" fill="white"></path>
    </svg>
</header>

<section id="container">
    <section id="actu">
        <h1>Actualités</h1>

        <!--
        En PHP, utiliser l'objet $pdo inséré dans ce fichier à la ligne "1" pour sélectionner toutes les actualitées contenues dans la base de données.
        Uiliser les éléments PHP récupérés pour afficher les articles comme sur l'image du sujet.

        Astuces pour les dates :
        $date = new DateTime();
        $date->setTimestamp('insérer ici la valeur de la colonne "date"');
        echo $date->format('Y/m/d H:i:s');
        -->


        <div class="articles">
            <?php foreach ($tt as $row):?>
            <article>
                <h2>
                    <?= $row['title']?>
                </h2>
                <div class="info">
                    <span class="author">
                        <?= $row['author']?>
                    </span>
                    <span class="date">
                        <?php
                        $date =  new DateTime();
                        $date->setTimestamp($row['date']);
                        echo $date->format('Y/m/d H:i:s');
                        ?>
                    </span>
                </div>
                <p>
                    <?= $row['text'] ?>
                </p>
            </article>
            <?php endforeach;?>
        </div>

    </section>

    <section id="info">
        <h1>Informations</h1>
        <section>
            <aside>
                <h3>Lieu</h3>
                <p>Le festival se déroulera au Zénith de Strasbourg.</p>
                <p>Accessible depuis les trams A et D.</p>
                <h3>Dates et horaires</h3>
                <p>Le festival se déroulera du 5 au 8 août inclus.</p>
                <p>Chaque soir le festival sera ouvert de 18h à 22h.</p>
            </aside>
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2638.8764278129065!2d7.684929816014564!3d48.59306487926239!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4796b7b7fa93a565%3A0x10a183f609a4fe1c!2sZ%C3%A9nith%20de%20Strasbourg!5e0!3m2!1sfr!2sfr!4v1575813745355!5m2!1sfr!2sfr"
                    width="600"
                    height="450"
                    frameborder="0"
                    style="border:0;"
                    allowfullscreen=""></iframe>
        </section>
    </section>

    <section id="contact">
        <h1>Contact</h1>
        <form action="contact.php" method="post">
            <header>
                <div>
                    <label for="firstName">Prénom*</label>
                    <input type="text" id="firstName" name="firstName"/>
                </div>
                <div>
                    <label for="lastName">Nom*</label>
                    <input type="text" id="lastName" name="lastName"/>
                </div>
            </header>
            <div id="div__subject">
                <label for="subject">Sujet*</label>
                <select id="subject" name="subject">
                    <option value="" selected style="display: none;">Sélectionner le sujet</option>
                    <option value="1">Achat de billet(s)</option>
                    <option value="2">Demande d'information</option>
                    <option value="3">Réclamation</option>
                </select>
            </div>
            <div id="div__message">
                <label for="message">Message*</label>
                <textarea name="message" id="message" cols="30" rows="10"></textarea>
            </div>
            <button type="submit">Envoyer</button>
        </form>
    </section>
</section>

<button id="to__top">
    <i class="mdi mdi-arrow-up-bold"></i>
</button>

<script src="./js/smooth-scroll.js"></script>
<script src="./js/button-top.js"></script>
<script src="./js/mouseover.js"></script>
</body>
</html>